%define name        jami-libclient
%define version     1.0.0.26ad683c
%define release     0
# just so we depend on the correct build:
%define daemon_version     10

Name:          %{name}
Version:       %{version}
Release:       %{release}%{?dist}
Summary:       Client library for Jami
Group:         Applications/Internet
License:       GPLv3+
Vendor:        Savoir-faire Linux
URL:           https://jami.net/
Source:        %{name}-%{version}.tar.gz
Patch0:        0001-guard-against-Q_NAMESPACE.patch
Requires:      jami-daemon >= %{daemon_version}, jami-daemon < %{daemon_version}.9999

# Build dependencies
BuildRequires: jami-daemon-devel >= %{daemon_version}, jami-daemon-devel < %{daemon_version}.9999
BuildRequires: jami-daemon >= %{daemon_version}, jami-daemon < %{daemon_version}.9999
BuildRequires: make
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: pkgconfig(Qt5Gui)
BuildRequires: pkgconfig(Qt5Sql)
BuildRequires: qt5-qttools-linguist

%description
This package contains the client library of Jami, a free software for
universal communication which respects freedoms and privacy of its
users.

%prep
%setup -n %{name}-%{version}/upstream
%patch0 -p1

%build
mkdir build 
pushd build
# note: apparently cmake matches case-sensitive for "false"
%cmake -DRING_BUILD_DIR=%{_builddir}/upstream/src \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DCMAKE_INSTALL_LIBDIR=%{_libdir} \
      -DCMAKE_BUILD_TYPE=Release \
      -DENABLE_STATIC=false \
      -DENABLE_VIDEO=false \
      -DENABLE_LIBWRAP=false \
          ..

make %{_smp_mflags}

%install
pushd build
%make_install

%files
%defattr(-,root,root,-)
%{_libdir}/libringclient.so.1.0.0
%{_datadir}/libringclient

%package devel
Summary: Development files of the Jami client library
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files and the unversioned shared
library for developing with the Jami client library.

%files devel
%{_includedir}/libringclient
%{_libdir}/cmake/LibRingClient
# The following is a symbolic link.
%{_libdir}/libringclient.so

%post
/sbin/ldconfig

%postun
/sbin/ldconfig
